import 'dart:async';

import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Provider(
        create: (BuildContext context) => InfiniteBloc(),
        child: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Text('Title', style: Theme.of(context).textTheme.headline1),
                Text(
                  'Subpart Title',
                  style: Theme.of(context).textTheme.headline2,
                ),
              ],
            ),
          ),
          InfiniteList(),
        ],
      ),
    );
  }
}

class InfiniteList extends StatefulWidget {
  @override
  _InfiniteListState createState() => _InfiniteListState();
}

class _InfiniteListState extends State<InfiniteList> {
  PagingController<int, String> _pagingController = PagingController(firstPageKey: 0,);
  StreamSubscription<ListingState<String>> _infiniteSubscription;

  @override
  void initState() {
    final InfiniteBloc bloc = Provider.of<InfiniteBloc>(context, listen: false);
    _pagingController.addPageRequestListener((pageKey) {
      bloc.fetch(pageKey);
    });

    // We could have used StreamBuilder, but that would unnecessarily recreate
    // the entire [PagedSliverGrid] every time the state changes.
    // Instead, handling the subscription ourselves and updating only the
    // _dataSource is more efficient.
    _infiniteSubscription =
        bloc.infiniteContent.listen((ListingState<String> listingState) {
          _pagingController.value = PagingState(
            nextPageKey: listingState.nextPageKey,
            error: listingState.error,
            itemList: listingState.list,
          );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PagedSliverList<int, String>.separated(
      separatorBuilder: (BuildContext context, _) => Divider(),
      pagingController: _pagingController,
      builderDelegate: PagedChildBuilderDelegate<String>(
        noItemsFoundIndicatorBuilder: (BuildContext context) => Padding(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Center(
            child: Text('No result'),
          ),
        ),
        itemBuilder: (
          BuildContext context,
          String item,
          int index,
        ) =>
            Card(child: Text(item)),
      ),
    );
  }

  @override
  void dispose() {
    _infiniteSubscription.cancel();
    _pagingController.dispose();
    super.dispose();
  }
}

class InfiniteBloc {
  final int _pageSize = 40;
  final BehaviorSubject<ListingState<String>> _infiniteContentController =
      BehaviorSubject<ListingState<String>>();

  Stream<ListingState<String>> get infiniteContent =>
      _infiniteContentController.stream;

  Future<void> fetch(int pageKey) async {
    await Future<void>.delayed(Duration(seconds: 5));
    final ListingState<String> old = _infiniteContentController.value;
    List<String> newList = <String>[];
    if (old != null && old.list != null) {
      newList = old.list;
    }
    newList.addAll(List<String>.generate(
        _pageSize, (int index) => 'Item ${pageKey*_pageSize + index}'));

    int nextPageKey = pageKey + 1;
    if (pageKey == 15) {
      nextPageKey = null;
    }
    _infiniteContentController.sink.add(
      ListingState<String>(
        error: null,
        list: newList,
        nextPageKey: nextPageKey,
      ),
    );
  }

  void dispose() {
    _infiniteContentController.close();
  }
}

class ListingState<T> {
  ListingState({this.list, this.nextPageKey, this.error});

  final List<T> list;
  final int nextPageKey;
  final dynamic error;
}
